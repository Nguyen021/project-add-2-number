import unittest
import my_big_number


class TestMyBigNumber(unittest.TestCase):
    def test_cal_sum1(self):
        result = my_big_number.cal_sum(self, "123", "32")
        self.assertEqual(result, "155")

    def test_cal_sum2(self):
        result = my_big_number.cal_sum(self, "1234", "897")
        self.assertEqual(result, "2131")

    def test_cal_sum3(self):
        result = my_big_number.cal_sum(self, "9999", "1")
        self.assertEqual(result, "10000")

    def test_cal_sum4(self):
        result = my_big_number.cal_sum(self, "12345678", "9876")
        self.assertEqual(result, "12355554")

    def test_cal_sum5(self):
        result = my_big_number.cal_sum(self, "123456789", "987654321")
        self.assertEqual(result, "1111111110")
