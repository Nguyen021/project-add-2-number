
def cal_sum(self, stn1: str, stn2: str) -> str:
        i, j = len(stn1) - 1, len(stn2) - 1
        temp = 0
        kq = []
        while i >= 0 or j >= 0:
            if i >= 0:
                num1 = int(stn1[i])
            else:
                num1 = 0
            if j >= 0:
                num2 = int(stn2[j])
            else:
                num2 = 0

            total = num1 + num2 + temp

            kq.insert(0, total % 10)

            print("Lấy {0} + {1} được {2}. Cộng tiếp với nhớ {3} được {4} Lưu {5} vào kết quả và nhớ {6}"
                  .format(num1, num2, num1 + num2, temp, total, total % 10, total // 10))
            temp = total // 10
            result = "".join(str(x) for x in kq)
            print("Kết quả là '{0}'".format(result))
            i -= 1
            j -= 1
        if temp > 0:
            print("Còn {0} ".format(temp))
            kq.insert(0, temp)
        print("Kết quả cuối cùng sẽ là '{0}' ".format("".join(str(x) for x in kq)))
        print("--------------------")
        return "".join(str(x) for x in kq)
