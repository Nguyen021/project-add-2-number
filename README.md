Để lấy project của tôi về chạy trên máy tính.

1. Mở Terminal hoặc Command Prompt và di chuyển đến thư mục bạn muốn clone project về.

Clone project bằng cách chạy lệnh sau:

git clone https://gitlab.com/Nguyen021/project-add-2-number.git

2. Kích hoạt môi trường ảo cho project với IDE pycharm

File --> Settings --> Project: project-add-2-number --> Python Interpreter --> Add Interpreter --> Ok 

3. Cài đặt các gói thư viện trong Terminal

pip install -r requirements.txt

4. Di chuyển vào thư mục code bằng lệnh

cd code

5. Chạy các unit test trong test_my_big_number.py bằng các lệnh sau

Lệnh 1. 
    python test_my_big_number.py 
Lệnh 2.
    python -m unittest test_my_big_number.py 


6. Add SSH KEY 
    Run CMD `ssh-keygen -t rsa`

    open file id_rsa.pub

